Carly Wood Mobile Wedding Hair Sydney has over 19 years experience in the wedding hair and makeup industry. Renowned for her passion for hair and with an undeniable reputation, Carly is one of Sydney’s elite bridal hairstylists with extensive experience in wedding hair and fashion shoots.

Address: 41 Bath Road, Kirrawee, NSW 2232

Phone: +61 418 961 291